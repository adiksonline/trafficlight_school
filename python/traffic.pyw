#! /usr/bin/python

from PyQt4.QtCore import *
from PyQt4.QtGui import *
import sys

class Traffic(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        self.setWindowTitle("Traffic Light Indicator")
        self.setMinimumSize(180, 420);
        self.setMaximumSize(180, 420)
        self.current = 0;
        self.switchState()

    def switchState(self):
        delay = 6000 if (self.current == 0 or self.current == 2) else 1000
        QTimer.singleShot(delay, self.callback)

    def callback(self):
        self.current = (self.current + 1) % 4
        self.repaint();
        self.switchState()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setPen(Qt.gray); painter.setBrush(Qt.NoBrush)
        painter.drawRect(20, 20, 140, 380)
        if (self.current == 0 or self.current == 1): painter.setPen(Qt.red)
        else: painter.setPen(Qt.gray)
        painter.setBrush(painter.pen().color());
        painter.drawEllipse(40, 40, 100, 100)
        if (self.current == 1 or self.current == 3): painter.setPen(Qt.yellow)
        else: painter.setPen(Qt.gray)
        painter.setBrush(painter.pen().color());
        painter.drawEllipse(40, 160, 100, 100)
        if (self.current == 2): painter.setPen(Qt.green)
        else: painter.setPen(Qt.gray)
        painter.setBrush(painter.pen().color());
        painter.drawEllipse(40, 280, 100, 100)

app = QApplication(sys.argv)
traffic = Traffic();
traffic.show()
app.exec_()
